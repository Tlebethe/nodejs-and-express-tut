//other patterns where we dont block the event loop

const {readFile, writeFile} = require('fs').promises

// readFile('./content/first.txt', 'utf8', (err, data)=> {
//   console.log('reading file...')
//   if (err) {
//     console.log('err happaned')
//   }
//   else {
//     console.log(data)
//   }
// })

//problem if we want to peform multiple stuff
// we can use a promise

// const getText = (path) => {
//   return new Promise((resolve , reject)=> {
//       readFile(path, 'utf8', (err, data)=> {
//   console.log('reading file...')
//   if (err) {
//     console.log('error ...')
//     reject(err)
//   }
//   else {
//     console.log('success')
//     resolve(data)
//   }
// })
//   })
// }
//we can chain the promise with  then and catch
// getText('./content/first.txt')
// .then((result)=> console.log(result))
// .catch((err)=> console.log(err))

//we dont need to nest , cleanier code



// const start = async() => {
//   try{
//     const first= await getText('./content/first.txt')
//     const second= await getText('./content/second.txt')
//     console.log(first, second)
//   }
//   catch(e)
//   {
//     console.log(e)
//   }

// }
// console.log('running start....\n')
// start()

//we will use another method to code gettetxt
const util = require('util')
// const readFilePromise = util.promisify(readFile)
// const writeFilePromise = util.promisify(writeFile)


const start = async() => {
  try{
    const first= await readFile('./content/first.txt', 'utf8')
    const second= await readFile('./content/second.txt', 'utf8')
    await writeFile('./content/bomb.txt', 'kaboooom!!!!')
    console.log(first, second)
  }
  catch(e)
  {
    console.log(e)
  }

}
console.log('running start....\n')
start()

